pyads==3.3.7
PyQt5==5.15.2
PyQt5-sip==12.9.0
PyQt5-stubs==5.15.2.0
pyqtconsole==1.2.1
numpy
pyqtgraph
paramiko
