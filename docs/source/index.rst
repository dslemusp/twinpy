.. TwinPy documentation master file, created by
   sphinx-quickstart on Tue Jan 12 09:24:16 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TwinPy's documentation!
==================================

TwinPy is a package containing tools to easily create your own Python GUIs for TwinCAT based on Simulink models.

The GUIs are based on PyQt5 and the TwinCAT interface is done through pyads.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Home <self>
   installation
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
