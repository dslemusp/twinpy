Modules
=======



TwinCAT and Simulink Interfacing
--------------------------------


TwinCAT Connection
~~~~~~~~~~~~~~~~~~

.. automodule:: twinpy.twincat.connection


Simulink
~~~~~~~~

.. automodule:: twinpy.twincat.simulink
    :no-members:

.. autofunction:: twinpy.twincat.simulink.sanitize_name

SimulinkModel
"""""""""""""

.. autoclass:: twinpy.twincat.SimulinkModel

SimulinkBlock
"""""""""""""

.. autoclass:: twinpy.twincat.SimulinkBlock


ADS Variables
~~~~~~~~~~~~~

.. automodule:: twinpy.twincat.symbols
    :no-members:

Symbol
""""""

.. autoclass:: twinpy.twincat.Symbol

Parameter
"""""""""

.. autoclass:: twinpy.twincat.Parameter

Signal
""""""

.. autoclass:: twinpy.twincat.Signal



GUI
---


TwinCAT UI Elements
~~~~~~~~~~~~~~~~~~~

.. automodule:: twinpy.ui.tc_widgets
    :no-members:

TcWidget
""""""""

.. autoclass:: twinpy.ui.TcWidget

TcLabel
"""""""

.. autoclass:: twinpy.ui.TcLabel

TcLineEdit
""""""""""

.. autoclass:: twinpy.ui.TcLineEdit

TcPushButton
""""""""""""

.. autoclass:: twinpy.ui.TcPushButton

TcRadioButton
"""""""""""""

.. autoclass:: twinpy.ui.TcRadioButton

TcRadioButtonGroupBox
"""""""""""""""""""""

.. autoclass:: twinpy.ui.TcRadioButtonGroupBox

TcCheckBox
""""""""""

.. autoclass:: twinpy.ui.TcCheckBox


Base GUI
~~~~~~~~

.. automodule:: twinpy.ui.base_gui
    :members:


Base Widgets
~~~~~~~~~~~~

.. automodule:: twinpy.ui.base_widgets
    :no-members:


TcErrorsLabel
"""""""""""""

.. autoclass:: twinpy.ui.TcErrorsLabel


Main - Example
--------------

.. automodule:: twinpy.__main__



Module Info
-----------

.. automodule:: twinpy
    :no-members:
