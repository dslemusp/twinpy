import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="twinpy-BE",  # Include username to prevent conflicts
    version="0.0.1",
    author="Robert Roos",
    author_email="robert.soor@gmail.com",
    description="Package to interface with TwinCAT (incl. Simulink models)",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/ctw-bw/wr-base-gui",
    packages=["twinpy"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
