from typing import Optional, Union, Type
import pyads
from pyads.constants import PLCDataType

from .symbols import Parameter, Signal
import socket


class TwincatConnection(pyads.Connection):
    """Extend default Connection object (typically named `plc`).

    ADS connection with custom features.
    """

    def __init__(
        self,
        CLIENT_IP: str = None, 
        TARGET_IP: str = None, 
        TARGET_PORT: int = 350, 
        TARGET_USERNAME: str = None,  
        TARGET_PASSWORD: str = None 
    ):
        """

        Note that this version will connect on object creation, throwing an exception
        when it fails. `pyads.Connection` waits for `.open()` and will fail quietly.

        :param CLIENT_IP: IP address of the computer running this library. Default 'None' for local. 
        :param TARGET_IP: IP address of computer running TwinCAT. Default 'None' for local. 
        :param TARGET_PORT: Port connecting to TC task. Default 350 for TwinCAT simulink models (For PLC projects check the PLC task in TC)
        :param TARGET_USERNAME: Twincat PC username (windows session). Default 'None' for local.
        :param TARGET_PASSWORD: Twincat PC password (windows session). Default 'None' for local.  
        :raises pyads.ADSError: When connection failed
        """

        self.TARGET_IP       = TARGET_IP #TwinCAT PC IP
        self.TARGET_USERNAME = TARGET_USERNAME 
        self.TARGET_PASSWORD = TARGET_PASSWORD
        self.TARGET_PORT     = TARGET_PORT
        
        if TARGET_IP is None or TARGET_IP == '127.0.0.1':          
            self.target_type    = "local"
            self.TARGET_NETID   = '127.0.0.1.1.1' #Desired NetID for this device
            # self.TARGET_IP      = TARGET_IP;            
        else:
            self.target_type = "remote"
            ## Connect to a remote pc with TwinCAT
            # CLIENT_IP       = socket.gethostbyname_ex(socket.gethostname())[-1][-1]
            CLIENT_IP       = self.get_ip() 
            CLIENT_NETID    = CLIENT_IP + '.1.1' #Desired NetID for this device
            ROUTE_NAME      = 'twinpy_route'
        
            # Add route from target to client (This is usually needed only once)
            pyads.add_route_to_plc( CLIENT_NETID,CLIENT_IP,self.TARGET_IP,self.TARGET_USERNAME,self.TARGET_PASSWORD,
                                    route_name = ROUTE_NAME)
            # Get target NETID
            self.TARGET_NETID    = pyads.ads.adsGetNetIdForPLC(self.TARGET_IP)                                    

        # TARGET_NETID    =  pyads.ads.adsGetNetIdForPLC(TARGET_IP)
        # print("Target NETID = {}" .format(TARGET_NETID))

        # Establish connection
        super().__init__(self.TARGET_NETID, self.TARGET_PORT, self.TARGET_IP)

        self.open()

        if not self.is_open:
            raise pyads.ADSError(
                text="Connection to TwinCAT could not be "
                "established. Is TwinCAT in run mode?"
                "Are the port and address correct?"
            )
    
    def get_ip(self):
        """" Get ip of PC where this library is running"""
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            # doesn't even have to be reachable
            s.connect(('10.255.255.255', 1))
            IP = s.getsockname()[0]
        except Exception:
            IP = '127.0.0.1'
        finally:
            s.close()
        return IP

    def get_module_info(self, module_name: str) -> dict:
        """Get information about live module."""

        # Read all info as list of bytes (doing it as a list of variable
        # names fails)
        try:
            var_name = module_name + ".ModuleInfo"
            data = self.read_by_name(var_name, pyads.PLCTYPE_ARR_DINT(37))
        except pyads.ADSError as err:
            # Re-raise error
            raise pyads.ADSError(
                text="Could not connect to TwinCAT module. "
                "Does the target module exist in the "
                "running TwinCAT instance?"
            ) from err

        return {
            "ClassId": data[0:4],
            "BuildTimeStamp": data[4],
            "ModelCheckSum": data[5:9],
            "ModelVersion": data[9:13],
            "TwinCatVersion": data[13:17],
            "TcTargetVersion": data[17:21],
            "MatlabVersion": data[21:25],
            "SimulinkVersion": data[25:29],
            "CoderVersion": data[29:33],
            "TcTargetLicenseID": data[33:38],
        }

    def get_symbol(
        self,
        name: Optional[str] = None,
        index_group: Optional[int] = None,
        index_offset: Optional[int] = None,
        plc_datatype: Optional[Union[Type["PLCDataType"], str]] = None,
        comment: Optional[str] = None,
        auto_update: bool = False,
    ) -> pyads.AdsSymbol:
        """

        See :meth:`pyads.Connection.get_symbol()`.

        Return an AdsSymbol object.

        :param name:
        :param index_group:
        :param index_offset:
        :param plc_datatype:
        :param comment:
        :param auto_update:
        :return:
        """
        return super().get_symbol(name, index_group, index_offset, plc_datatype)

    def get_signal(
        self,
        name: Optional[str] = None,
        index_group: Optional[int] = None,
        index_offset: Optional[int] = None,
        symbol_type: Optional[Union[str, Type]] = None,
    ) -> Signal:
        """Get Signal instance.

        See :class:`Signal`.
        """
        return Signal(
            plc=self,
            name=name,
            index_group=index_group,
            index_offset=index_offset,
            symbol_type=symbol_type
        )

    def get_parameter(
        self,
        name: Optional[str] = None,
        index_group: Optional[int] = None,
        index_offset: Optional[int] = None,
        symbol_type: Optional[Union[str, Type]] = None,
    ) -> Parameter:
        """Get Parameter instance.

        See :class:`Parameter`.
        """
        return Parameter(
            plc=self,
            name=name,
            index_group=index_group,
            index_offset=index_offset,
            symbol_type=symbol_type
        )
