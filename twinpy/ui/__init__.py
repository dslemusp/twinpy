"""TwinPy.UI module.

These imports allow the user to skip the file names in their import.
"""

from .tc_widgets import (
    TcWidget,
    TcLabel,
    TcPushButton,
    TcLineEdit,
    TcRadioButton,
    TcRadioButtonGroupBox,
    TcCheckBox,
    TcSlider,
    TcGraph,
)
from .base_gui_cmg import CMGGUI
from .base_gui import TcMainWindow, BaseGUI
from .base_widgets import TcErrorsLabel, ErrorPopupWindow
