"""Module containing the Base GUI class.

This class should be extended to make your own GUI.
"""


from typing import List, Optional
import os

from PyQt5.QtWidgets import (
    QMainWindow,
    QWidget,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QFormLayout,
    QGroupBox,
)
from PyQt5.QtGui import QIcon

try:
    from pyqtconsole.console import PythonConsole
except ModuleNotFoundError:
    PythonConsole = None  # Make console optional

from .tc_widgets import TcWidget, TcPushButton, TcLabel
from .base_widgets import TcErrorsLabel
from ..twincat.simulink import SimulinkModel

try:
    import ctypes

    APP_NAME = "WR.BaseGui"  # Change application id to make the taskbar icon
    # displayed correctly
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(APP_NAME)
except ImportError:
    ctypes = None  # Very unimportant fix, continue if ctypes was not found
except AttributeError:
    pass  # On Linux systems, windll is not available and will give trouble


class TcMainWindow(QMainWindow):
    """Parent class for TwinCAT GUIs.

    Extends QMainWindow. The resulting window is empty, but will have a
    destructor that neatly closes any TcWidgets first.

    To make it easier to navigate the different elements, adhere to:

    * Create objects as late as possible
    * Save objects as property only if that is really necessary
    * Name objects by elements starting with the most general item
      (e.g. 'layout_group_drives')
    * To save space, create a Layout directly with its parent widget:

            `button_layout = QLayout(widget_parent)`

            `# widget_parent.addLayout(button_layout)  # < Not needed now`

    Widgets consist of layouts. Layouts contain widgets.
    """

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self.setWindowTitle("TwinCAT GUI")

        icon_path = os.path.realpath("resources/icon.ico")
        self.setWindowIcon(QIcon(icon_path))

    def find_tc_widgets(self) -> List[TcWidget]:
        """Find all children of the TcWidget type (recursively)."""
        for widget in self.findChildren(TcWidget):
            yield widget

    def closeEvent(self, event):
        """On window close"""

        # An error will occur when a callback is fired to a widget that has
        # already been removed, so on the closing of the window we make sure
        # to clear callbacks to TcWidgets
        for widget in self.find_tc_widgets():
            widget.connect_symbol(None)

        super().closeEvent(event)  # Continue to parent event handler


class BaseGUI(TcMainWindow):
    """Base TwinCAT GUI, specific for the WE2 actuators and controller model.

    Extends this class if you want to use those models. For other models,
    using :class:`twinpy.ui.TcMainWindow` might be more appropriate.
    """

    def __init__(
        self,
        actuator: Optional[SimulinkModel] = None,
        controller: Optional[SimulinkModel] = None,
        **kwargs
    ):
        """

        :param actuator: The WE2_actuators model (or a derivative)
        :param controller: The WE2_controller model (or a derivative)
        :param kwargs:
        """

        super().__init__()

        main_widget = QWidget()  # Widget spanning the entire window
        self.layout_main = QVBoxLayout(main_widget)  # Main button_layout
        self.setCentralWidget(main_widget)  # Make widget the main thingy

        if controller is not None and hasattr(controller, "Controller"):
            controller = controller.Controller  # Shorten one block

        # ------- Motor Drives -------

        self.button_drives_enable = TcPushButton("Enable Drives")
        self.button_drives_disable = TcPushButton("Disable Drives")
        self.label_drives_enabled = TcLabel()
        self.button_calibrate = TcPushButton("Calibrate motor encoders")

        if controller is not None:
            self.button_drives_enable.connect_symbol(
                controller.EnableAllDrives_Button.Value
            )
            self.button_drives_disable.connect_symbol(
                controller.DisableAllDrives_Button.Value
            )
            self.button_calibrate.connect_symbol(  # Typo is correct
                controller.CalibrateMotorEncoderss_Button.Value
            )

            # We use a manual callback for the drives instead of formatter:
            controller.OperationEnabled_Read.so1.add_device_notification(
                self.on_drives_enabled_change
            )

        group_drives = QGroupBox("Drives")
        layout_group_drives = QVBoxLayout(group_drives)
        layout_group_drives.addWidget(self.button_drives_enable)
        layout_group_drives.addWidget(self.button_drives_disable)
        layout_group_drives.addWidget(self.button_calibrate)
        layout_group_drives.addWidget(self.label_drives_enabled)

        self.layout_main.addWidget(group_drives)

        # ------- Errors -------

        self.label_errors_current = TcErrorsLabel()
        self.label_errors_current.popup_window.setWindowTitle("Current Errors")
        self.label_errors_last = TcErrorsLabel()
        self.label_errors_last.popup_window.setWindowTitle("Last Errors")

        if controller is not None:
            self.label_errors_current.connect_symbol(
                controller.JointErrorCurrent_Read.so1
            )
            self.label_errors_last.connect_symbol(controller.JointErrorLatched_Read.so1)

        group_errors_current = QGroupBox("Current Errors")
        group_errors_last = QGroupBox("Last Errors")

        layout_group_errors_current = QVBoxLayout(group_errors_current)
        layout_group_errors_last = QVBoxLayout(group_errors_last)
        layout_group_errors_current.addWidget(self.label_errors_current)
        layout_group_errors_last.addWidget(self.label_errors_last)

        layout_errors = QHBoxLayout()
        layout_errors.addWidget(group_errors_current)
        layout_errors.addWidget(group_errors_last)
        self.layout_main.addLayout(layout_errors)

        # ------- System info -------

        self.label_battery_logic = TcLabel()
        self.label_battery_motor = TcLabel()
        self.label_temperature = TcLabel()

        if actuator is not None:
            self.label_battery_logic.connect_symbol(actuator.LogicBatteryVoltage_V.so1)
            self.label_battery_motor.connect_symbol(actuator.MotorBatteryVoltage_V.so1)
            self.label_temperature.connect_symbol(actuator.BackpackTemperature_V.so1)

        group_system = QGroupBox("System Info")
        layout_group_system = QFormLayout(group_system)
        layout_group_system.addRow(
            QLabel("Logic Battery (V):"), self.label_battery_logic
        )
        layout_group_system.addRow(
            QLabel("Motor Battery (V):"), self.label_battery_motor
        )
        layout_group_system.addRow(
            QLabel("Backpack Temperature (C):"), self.label_temperature
        )

        self.layout_main.addWidget(group_system)

        # ------- Console -------

        if PythonConsole is not None:
            self.console = PythonConsole()
            self.layout_main.addWidget(self.console)
            self.console.push_local_ns("actuator", actuator)
            self.console.push_local_ns("controller", controller)
            self.console.eval_in_thread()
        else:
            self.console = None

        # -------

        self.show()

    def on_drives_enabled_change(self, enabled_list: List[float]):
        """An additional callback for the drive state change.

        Manual callback instead of TcWidget symbol connection so we can also
        change button state and label color.
        """

        drives_count = len(enabled_list)
        enabled_count = sum(e > 0 for e in enabled_list)

        if enabled_count == 0:
            label_text = "All drives are disabled"
            style = ""
            button_enable_on = True
            button_disable_on = False
        elif enabled_count == drives_count:
            label_text = "All drives are enabled"
            style = "background-color:#14DB4C"
            button_enable_on = False
            button_disable_on = True
        else:
            label_text = "%d out of %d drives enabled" % (enabled_count, drives_count)
            style = "background-color:#74CC8D"
            button_enable_on = True
            button_disable_on = True

        self.label_drives_enabled.setText(label_text)
        self.label_drives_enabled.setStyleSheet(style)
        self.button_drives_enable.setEnabled(button_enable_on)
        self.button_drives_disable.setEnabled(button_disable_on)

    def closeEvent(self, event):
        """Callback when window is closed."""

        # Also close error popup windows, just in case

        if self.label_errors_current is not None:
            if self.label_errors_current.popup_window is not None:
                self.label_errors_current.popup_window.close()

        if self.label_errors_last is not None:
            if self.label_errors_last.popup_window is not None:
                self.label_errors_last.popup_window.close()

        super().closeEvent(event)
