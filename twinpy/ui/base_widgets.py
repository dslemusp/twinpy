"""These widgets are specific implementations of TcWidgets.

They are not intended to be overridden again. They are separate classes mostly
because their specific logic became significant.
"""

from typing import List, Optional

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout, QScrollArea
from PyQt5.QtGui import QMouseEvent

from .tc_widgets import TcLabel


class TcErrorsLabel(TcLabel):
    """Extension of TcLabel for a list of joint errors.

    This is separate class because the amount of logic got a little bit much.

    When clicking on the widget, a new window pops up showing the decoded errors.
    """

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:

        :kwargs:
            * `format`: Callback to format errors (default: show hexadecimal representation
                        of error values
            * `popup`: Whether or not to enable a detailed popup window (default: True)
            * See :class:`TcLabel`
        """

        # Use local format function (neatly attached to format callback)
        if "format" not in kwargs:
            kwargs["format"] = self.format_errors_list

        self.popup_window: Optional[ErrorPopupWindow] = None
        if kwargs.pop("popup", True):
            self.popup_window = ErrorPopupWindow()

        super().__init__(*args, **kwargs)

        self.setTextFormat(Qt.RichText)  # Allow HTML-like tags

    def twincat_receive(self, value):
        """Callback on remote value change."""

        if self.popup_window is not None:
            # Update window content
            self.popup_window.update_content(value)

        # Base method does a great job already:
        super().twincat_receive(value)

    @staticmethod
    def format_errors_list(error_list: List[int]) -> str:
        """Set text for errors label."""

        text = ""

        for error in error_list:
            if text:
                text += "<br>"  # In rich text, use HTML break instead of \n

            code = TcErrorsLabel.to_hex(error)

            if error > 0:
                line = "<font color=red><b>" + code + "</b></font>"
            else:
                line = "<font color=grey>" + code + "</font>"

            text += line

        return text

    @staticmethod
    def to_hex(value: int) -> str:
        """Create human-readable hex from integer."""

        code = hex(abs(value)).upper()[2:].rjust(8, "0")

        code = code[0:4] + " " + code[4:]  # Add a space for readability

        return code

    def mousePressEvent(self, event: QMouseEvent) -> None:
        """On clicking on the label.

        QLabel does not have an on-click signal already.
        """

        win = self.popup_window  # Create shortcut reference
        if win is not None:
            win.show()
            win.activateWindow()
            win.setWindowState(
                int(win.windowState()) & ~Qt.WindowMinimized | Qt.WindowActive
            )

        super().mousePressEvent(event)


class ErrorPopupWindow(QWidget):
    """Popup window for drive error details.

    It's meant to only be instantiated by :class:`TcErrorsLabel`.

    This window does not attach it's own ADS callbacks. Instead it must be called
    by another widget that does.
    """

    ERROR_DESCRIPTIONS = [
        "ActuatorNotInUse",
        "Slave offline",
        "Motor angle guard",
        "JointAngle position guard",
        "Spring deflection guard",
        "Torque guard",
        "Motor encoder frozen",
        "Joint encoder frozen",
        "Spring encoder frozen",
        "Encoder consistency",
        "FromToActuator consistency",
        "Drive went off without error",
        "STO active (with or without Stop button)",
        "Motor overvoltage",
        "Motor undervoltage",
        "Drive over/under-temperature",
        "Motor overtemperature",
        "Overcurrent/Short circuit error (drive limit)",
        "Overcurrent (user limit)",
        "I2t error",
        "Position out of range detected by drive",
        "Velocity out of range detected by drive",
        "Drive communications watchdog error",
        "Too many encoder read errors",
        "Drive external fault",
        "Drive configuration error",
        "Drive electronics problem",
        "Other drive error (see LastError register)",
    ]

    JOINTS = ["LHA", "LHF", "LK", "LA", "RHA", "RHF", "RK", "RA"]

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.setWindowTitle("Drive Errors")

        # Create read-only label and make it the main thing
        self.label = ScrollLabel()

        self.main_layout = QVBoxLayout(self)
        self.main_layout.addWidget(self.label)

    def update_content(self, error_list):
        """Set the window content based on a new errors list."""

        text = ""

        for i, error in enumerate(error_list):
            if text:
                text += "<br>"  # In rich text, use HTML break instead of \n

            text += "<u>" + self.JOINTS[i] + "</u>: &nbsp;"

            code = TcErrorsLabel.to_hex(error)

            if error > 0:
                text += "<font color=red><b>" + code + "</b></font>"
            else:
                text += "<font color=grey>" + code + "</font>"

            text += "<br>"

            for descriptions in self.get_error_descriptions(error):
                text += " - " + descriptions + "<br>"

        self.label.setText(text)

    @classmethod
    def get_error_descriptions(cls, error: int) -> List[str]:
        """Get list of decoded errors from an error code"""

        if error == 0:
            return []  # Save some effort

        descriptions = []

        for bit, description in enumerate(cls.ERROR_DESCRIPTIONS):
            # Perform bitwise check

            mask = 2 ** bit
            if error & mask:
                text = TcErrorsLabel.to_hex(mask) + " | " + description
                descriptions.append(text)

        return descriptions


class ScrollLabel(QScrollArea):
    """Scrollable label."""

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.setWidgetResizable(True)

        content = QWidget(self)
        self.setWidget(content)
        layout = QVBoxLayout(content)

        self.label = QLabel("NaN")
        self.label.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.label.setWordWrap(True)
        self.label.setTextFormat(Qt.RichText)  # Allow HTML-ish formatting
        self.label.setTextInteractionFlags(Qt.TextSelectableByMouse)

        layout.addWidget(self.label)

    def setText(self, text):  # noqa: N802 # pylint: disable=invalid-name
        self.label.setText(text)


