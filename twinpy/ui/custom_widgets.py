from PyQt5.QtGui import QGuiApplication
from PyQt5.QtWidgets import QWidget, QVBoxLayout
from pyqtgraph import PlotWidget, QtGui
from numpy import zeros, roll

class QGraph(QWidget):
    "Plot widget "
    def __init__(self,buffer_size = 100, *args, **kwargs):
    # def __init__(self, *args, **kwargs):
        """
        buffer_size: (int) indicate de size of the plotting buffer in samples.
                     This has a direct impact in the sliding window in the plot
        """
        super().__init__(*args, **kwargs)

        # buffer_size = kwargs.pop('buffer_size', 100)

        layout = QVBoxLayout()

        # Create plotting object
        buffer_size = 100
        self.data = zeros(buffer_size)
        self.plot = PlotWidget()
        self.curve = self.plot.getPlotItem().plot()
        self.curve.setData(self.data)
        
        # Add plotting object to layout
        layout.addWidget(self.plot)
        self.setLayout(layout)

    def update_plot(self,value):
        "Updates plot appending new value at the end"
        # Shift data to the left (Moves first value to the end)
        self.data = roll(self.data,-1)
        # Replaces last value with new value
        self.data[-1] = value
        # Update ploted curve
        self.curve.setData(self.data)
        # QtGui.QApplication.processEvents()
        QGuiApplication.processEvents()


