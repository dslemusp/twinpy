# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Development

## 1.1.1

Added

 * Possible error beep for TcErrorsLabel, enabled for DriveErrorsWidget.
 * Default symbol event options.
 * `TcGraph` rolling graph widget.

Changed

 * Drives and Error widgets now use the actuator model instead of controller model, to be more flexible. This will require the latest actuator model version! 

## 1.1.0

Added

 * Widgets are disabled/enabled based on their connection status.

Changed

 * Base GUI elements have been turned into widgets for modularity in custom GUIs.
 * The Base GUI is split between regular items on the left and a collection of tabs on the right to ease adding custom elements.

## 1.0.2

Added

 * [#24](https://bitbucket.org/ctw-bw/twinpy/pull-requests/24) Added recursive print to display an entire model.

## 1.0.1

### Added

 * [#23](https://bitbucket.org/ctw-bw/twinpy/pull-requests/23) Added timed event as alternative to callbacks.

## 1.0.0

First tagged version, containing the development from October 2020 to June 2021.