import unittest
from unittest import mock
import os
import pyads

from twinpy.twincat.simulink import sanitize_name
from twinpy.twincat import SimulinkModel, SimulinkBlock, Parameter, Signal


current_dir = os.path.dirname(os.path.realpath(__file__))


# Use mock to point the TwinCAT directory to the local directory
@mock.patch.dict(os.environ, {"TWINCAT3DIR": current_dir})
class SimulinkTest(unittest.TestCase):
    """Test the simulink classes.

    These tests are far from complete. No TwinCAT / Simulink is needed, so far
    the tests are only superficial.
    """

    def test_model(self):
        """Test model creation by name.

        And roughly confirm the button_layout of the model.
        """

        model = SimulinkModel(0, "MyModel", "Controller_basic")
        self.assertEqual("MyModel", model.name)

        self.assertIsInstance(model.Sum, SimulinkBlock)
        self.assertIsInstance(model.ArrayConstant, SimulinkBlock)
        self.assertIsInstance(model.MySineWave, SimulinkBlock)
        self.assertIsInstance(model.MySubsystem.MyOffset2, SimulinkBlock)

    def test_model_by_dir(self):
        """Test model creation by directory."""

        model_dir = os.path.join(current_dir, "CustomConfig/Modules/Controller_basic")
        model = SimulinkModel(0, "MyModel", model_dir)  # Create with full path
        # If no errors occur it will be fine
        self.assertEqual("MyModel", model.name)
        self.assertIsInstance(model.Sum, SimulinkBlock)

    def test_model_by_file(self):
        """Test model creation by file path."""

        model_path = os.path.join(
            current_dir,
            "CustomConfig/Modules/Controller_basic/deploy"
            "/Controller_basic_ModuleInfo.xml",
        )
        model = SimulinkModel(0, "MyModel", model_path)  # Create with full path
        # If no errors occur it will be fine
        self.assertEqual("MyModel", model.name)
        self.assertIsInstance(model.Sum, SimulinkBlock)

    def test_sanitize(self):
        """Test the name sanitization."""

        words_list = [
            ("invalid_*cha ra&cter", "invalid_character"),
            ("7leadingDigit", "leadingDigit"),
            ("_leadingUnderscoRe", "leadingUnderscoRe"),
            ("_77_0__leading_mess", "leading_mess"),
        ]

        for words in words_list:
            self.assertEqual(words[1], sanitize_name(words[0]))

    def test_symbols(self):
        """Test symbols in model."""

        model = SimulinkModel(0x01010010, "Controller_basic")

        self.assertIsInstance(model.MyConstant.Value, Parameter)
        self.assertIsInstance(model.MySineWave.Amplitude, Parameter)
        self.assertIsInstance(model.MySineWave.Frequency, Parameter)
        self.assertIsInstance(model.Sum2.so1, Signal)

        symbol = model.MyConstant.Value
        self.assertEqual(symbol.plc_type, pyads.PLCTYPE_LREAL)
        self.assertEqual(symbol.index_group, 0x01010010)
        self.assertEqual(symbol.index_offset, 0x82000000)  # Values read from real
        # TwinCAT solution

        self.assertIsNone(symbol._plc)  # No connection yet

    def test_print_structure(self):
        """Test recursive debug print."""

        model = SimulinkModel(0x01010010, "Controller_basic")

        model.print_structure(max_depth=1)


if __name__ == "__main__":
    unittest.main()
