This directory is a fake root for `C:/TwinCAT/3.1/`, containing the relevant files of a compiled Simulink model.

The original model `Controller_basic.slx` is also included.
