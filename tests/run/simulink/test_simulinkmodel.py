"""
Create a Simulink interface object, verify the blocks are available directly
"""

from twinpy.twincat import SimulinkModel

# model = SimulinkModel(0x01010010, 'Controller')
model = SimulinkModel(0x101010, "WE2_actuator_model", "we2_actuator_model")

print(model.name)

# print(model.MySubsystem)

for symbol in model.get_symbols_recursive():
    print(symbol)
