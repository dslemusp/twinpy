from twinpy.twincat.simulink import sanitize_name

names = ["derp", "some_name@stuff", "123something456+"]

for name in names:
    print(sanitize_name(name))
