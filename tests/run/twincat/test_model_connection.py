"""
Create a Simulink interface object and connect it to a running TwinCAT
"""

import pyads
from twinpy.twincat import SimulinkModel
from twinpy.twincat import TwincatConnection

# import random

# model = SimulinkModel(0x01010010, 'Controller')
model = SimulinkModel(0x01010010, "WE2_actuator_model", "we2_actuator_model")

tc = TwincatConnection()

model.connect_to_twincat(tc)

# print(model.MatrixConstant.get())

# model.ArrayConstant.set([random.uniform(0, 10), random.uniform(0, 10)])
# print(model.ArrayConstant.get())
