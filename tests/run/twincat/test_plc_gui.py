"""
TwinPy GUI can also be used without a Simulink model.
"""

from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QFormLayout, QLabel
import sys
from pyads import ADSError

from twinpy.ui import TcMainWindow, TcLabel
from twinpy.twincat import TwincatConnection, Parameter


class GUI(TcMainWindow):
    """Some extension of the BaseGUI.

    Custom GUIs should extend the base class.
    """

    def __init__(self, connection: TwincatConnection):

        super().__init__()

        main_widget = QWidget()  # Widget spanning the entire window
        self.layout_main = QVBoxLayout(main_widget)  # Main button_layout
        self.setCentralWidget(main_widget)  # Make widget the main thingy

        self.symbol_list = Parameter(plc=connection, name="GVL.my_list")

        self.label_list = TcLabel(symbol=self.symbol_list)

        layout_form = QFormLayout()
        layout_form.addRow(QLabel("List:"), self.label_list)

        self.layout_main.addLayout(layout_form)

        self.show()


def main():

    app = QApplication(sys.argv)

    try:
        connection = TwincatConnection(ams_net_port=851)
    except ADSError as err:
        connection = None
        print("Not connected to TwinCAT, continuing -", err)

    gui = GUI(connection)

    app.exec()


if __name__ == "__main__":
    main()
