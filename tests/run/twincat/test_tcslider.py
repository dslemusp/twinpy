import sys
from PyQt5.QtWidgets import QApplication, QFormLayout, QLabel, QWidget, QSlider
from PyQt5.QtCore import Qt
from typing import Dict

from twinpy.ui import TcMainWindow, TcSlider, TcLabel
from twinpy.twincat import TwincatConnection


class TestGUI(TcMainWindow):
    def __init__(self, model: Dict, **kwargs):

        super().__init__(**kwargs)

        main_widget = QWidget()
        layout_form = QFormLayout(main_widget)

        self.slider = TcSlider(
            symbol=model['my_double'],
            event_type=TcSlider.EVENT_TIMER,
            update_freq=1.0,
            min=0.0,
            max=5.0,
            interval=0.5,
            orientation=Qt.Vertical,
            show_value=False,
        )
        self.slider.slider.setTickPosition(QSlider.TicksRight)
        self.label = TcLabel(
            symbol=model['my_double'],
        )

        self.slider2 = TcSlider(
            symbol=model['my_int'],
            min=0,
            max=20,
            interval=5,
            show_labels=False,
        )
        self.slider2.slider.setTickPosition(QSlider.TicksBelow)
        self.label2 = TcLabel(
            symbol=model['my_int'],
        )

        layout_form.addRow(QLabel("my_double:"), self.slider)
        layout_form.addRow(QLabel("my_double:"), self.label)
        layout_form.addRow(QLabel("my_int:"), self.slider2)
        layout_form.addRow(QLabel("my_int:"), self.label2)

        self.setCentralWidget(main_widget)
        self.show()


def main():
    tc = TwincatConnection(ams_net_port=851)

    app = QApplication(sys.argv)

    model = {
        'my_double': tc.get_parameter('GVL.my_double'),
        'my_int': tc.get_parameter('GVL.my_int')
    }

    gui = TestGUI(model)

    app.exec()


if __name__ == "__main__":
    main()
