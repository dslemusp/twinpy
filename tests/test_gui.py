import sys
import unittest
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

from twinpy.ui import BaseGUI

app = QApplication(sys.argv)


class BaseGUITest(unittest.TestCase):
    """Test the BaseGUI class."""

    def setUp(self) -> None:
        """Create the GUI."""
        self.gui = BaseGUI()
        self.gui.widget_errors.label_errors_current.setDisabled(False)  # Override state

    def tearDown(self) -> None:
        """Destroy GUI again."""
        self.gui = None

    def test_stuff(self):

        label = self.gui.widget_errors.label_errors_current

        self.assertTrue(label.popup_window.isHidden())

        QTest.mouseClick(label, Qt.LeftButton)

        self.assertFalse(label.popup_window.isHidden())


if __name__ == "__main__":
    unittest.main()
