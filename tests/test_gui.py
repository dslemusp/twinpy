import sys
import unittest
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

from twinpy.ui import BaseGUI

app = QApplication(sys.argv)


class BaseGUITest(unittest.TestCase):
    """Test the BaseGUI class."""

    def setUp(self) -> None:
        """Create the GUI."""
        self.gui = BaseGUI()

    def tearDown(self) -> None:
        """Destroy GUI again."""
        self.gui = None

    def test_stuff(self):

        self.assertTrue(self.gui.label_errors_current.popup_window.isHidden())

        QTest.mouseClick(self.gui.label_errors_current, Qt.LeftButton)

        self.assertFalse(self.gui.label_errors_current.popup_window.isHidden())


if __name__ == "__main__":
    unittest.main()
