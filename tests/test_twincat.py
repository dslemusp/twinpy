import unittest

from tests.twincat_testcase import TwinCatTestCase


class TwinCATTest(TwinCatTestCase):
    """Test the simulink classes."""

    def test_model_connection(self):
        """Test Simulink model when connected to TwinCAT.

        The module info and the specific symbol had been created in the test-case
        setup.
        """

        self.assertIsNone(self.model.MyConstant.Value._plc)

        self.model.connect_to_twincat(self.plc)

        self.assertIsNotNone(self.model.MyConstant.Value._plc)

        self.model.MyConstant.Value.write(15)
        value = self.model.MyConstant.Value.get()
        self.assertEqual(15, value)


if __name__ == "__main__":
    unittest.main()
