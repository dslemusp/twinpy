import unittest
import abc
from unittest import mock
import os
import time
import pyads
import struct
from pyads import constants
from pyads.testserver import AdsTestServer, AdvancedHandler, PLCVariable
from PyQt5.QtCore import QEventLoop, QTimer

from twinpy.twincat import SimulinkModel, TwincatConnection


current_dir = os.path.dirname(os.path.realpath(__file__))


class TwinCatTestCase(unittest.TestCase, metaclass=abc.ABCMeta):
    """Abstract base test-case for TwinCAT related stuff.

    TWINCAT3DIR is mock patched for the entire case.
    """

    # These are pretty arbitrary
    TEST_SERVER_AMS_NET_ID = "127.0.0.1.1.1"
    TEST_SERVER_IP_ADDRESS = "127.0.0.1"
    TEST_SERVER_AMS_PORT = 48898

    @classmethod
    def setUpClass(cls):
        """Setup the ADS test server."""

        cls.handler = AdvancedHandler()
        cls.test_server = AdsTestServer(
            cls.handler,
            cls.TEST_SERVER_IP_ADDRESS,
            cls.TEST_SERVER_AMS_PORT,
            logging=False,
        )
        cls.test_server.start()

        # wait a bit otherwise error might occur
        time.sleep(1)

    @classmethod
    def tearDownClass(cls):
        """Tear down the test server."""
        cls.test_server.stop()

        # wait a bit for server to shutdown
        time.sleep(1)

    def setUp(self):
        """Establish connection to the test server."""

        # Clear test server and handler
        self.test_server.request_history = []
        self.handler.reset()

        # Create PLC variable that is added by default
        self.test_var = PLCVariable(
            name="Controller_basic.ModelParameters.MyConstant_value",
            value=bytes(8),
            ads_type=constants.ADST_REAL64,
            symbol_type="LREAL",
        )
        self.test_var.index_group = 16842768
        self.test_var.index_offset = 2181038080
        self.test_var.comment = "Some variable of type double"
        self.test_var_type = pyads.constants.PLCTYPE_LREAL  # Corresponds with "LREAL"
        self.handler.add_variable(self.test_var)

        data = [0] * 37
        data[5:9] = [-536987094, -1003294337, 674671662, -1365606702]  # Checksum
        module_info = struct.pack("i" * len(data), *data)

        self.handler.add_variable(
            PLCVariable(
                name="Controller_basic.ModuleInfo",
                value=module_info,
                ads_type=constants.ADST_INT32,
                symbol_type="ARRAY [1..37] OF DINT",
            )
        )

        # Env variables are mocked
        with mock.patch.dict(os.environ, {"TWINCAT3DIR": current_dir}):
            self.model = SimulinkModel(0x1010010, "Controller_basic")

        self.plc = TwincatConnection(
            self.TEST_SERVER_AMS_NET_ID,
            self.TEST_SERVER_AMS_PORT,
            self.TEST_SERVER_IP_ADDRESS,
        )

        # No connection to the model is made yet!

    def tearDown(self) -> None:
        """Break down after each test."""

        self.plc.close()  # Ensure connection is closed first

        del self.model
        del self.plc

    @staticmethod
    def sleep_qt(delay_ms: int):
        """Perform a PyQt-friendly wait.

        The regular time.sleep() will not allow event processing by QT.

        :param delay_ms: Sleep time in milliseconds
        """
        loop = QEventLoop()
        QTimer.singleShot(delay_ms, loop.quit)
        loop.exec_()
