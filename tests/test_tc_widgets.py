import unittest
from unittest import mock
import sys
import abc
import pyads
import numpy as np
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

from twinpy.ui import (
    TcMainWindow,
    TcWidget,
    TcLabel,
    TcLineEdit,
    TcPushButton,
    TcRadioButton,
    TcRadioButtonGroupBox,
    TcCheckBox,
    TcSlider,
    TcGraph,
)

from tests.twincat_testcase import TwinCatTestCase


app = QApplication(sys.argv)


class GUI(TcMainWindow):
    """Empty GUI to test widgets"""

    def __init__(self):

        super().__init__()

        main_widget = QWidget()
        self.main_layout = QVBoxLayout(main_widget)
        self.setCentralWidget(main_widget)


class GuiTestCase(TwinCatTestCase, metaclass=abc.ABCMeta):
    """TestCase for TcWidget classes."""

    def setUp(self):
        """Establish connection to the test server."""

        super().setUp()

        # Connect model
        self.model.connect_to_twincat(self.plc)

        # Create GUI
        self.gui = GUI()
        self.gui.show()

    def tearDown(self) -> None:
        """Break down test objects."""

        self.gui.close()  # Force close event (not triggered otherwise, will let
        # notifications linger which is bad)

        # Destroy objects in opposite order of creation
        del self.gui

        super().tearDown()


class TcLabelTestCase(GuiTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.real_defaults = (TcWidget.DEFAULT_EVENT_TYPE, TcWidget.DEFAULT_UPDATE_FREQ)

    def setUp(self):
        super().setUp()

        # Create label around parameter from model
        self.label = TcLabel()
        self.gui.main_layout.addWidget(self.label)

    def tearDown(self):
        super().tearDown()

        TcWidget.DEFAULT_EVENT_TYPE, TcWidget.DEFAULT_UPDATE_FREQ = self.real_defaults

    def test_tc_label(self):
        """Test TcLabel class."""

        self.assertEqual("NaN", self.label.text())  # Default text

        self.label.connect_symbol(self.model.MyConstant.Value)  # Now connect the symbol

        index_group = self.model.MyConstant.Value.index_group
        index_offset = self.model.MyConstant.Value.index_offset
        self.plc.write(index_group, index_offset, 3.14, pyads.PLCTYPE_LREAL)

        self.assertEqual("3.140", self.label.text())  # Confirm label got updated

    def test_tc_label_time_event(self):
        """Test TcLabel class with the timer events."""

        # Now connect the symbol
        self.label.connect_symbol(
            new_symbol=self.model.MyConstant.Value,
            event_type=TcLabel.EVENT_TIMER,
            update_freq=100.0,
        )

        index_group = self.model.MyConstant.Value.index_group
        index_offset = self.model.MyConstant.Value.index_offset
        self.plc.write(index_group, index_offset, 3.14, pyads.PLCTYPE_LREAL)

        self.assertEqual("NaN", self.label.text())  # Still the default text

        self.sleep_qt(100)

        self.assertEqual("3.140", self.label.text())  # Confirm label got updated

    def test_tc_widget_default(self):
        """Test changing the widget defaults."""

        # With default defaults
        self.label.connect_symbol(self.model.MyConstant.Value)

        self.assertIsNone(self.label._timer)

        # Change defaults
        TcWidget.DEFAULT_EVENT_TYPE = TcWidget.EVENT_TIMER
        TcWidget.DEFAULT_UPDATE_FREQ = 164.54  # Hz

        self.label = TcLabel()  # Create new instance to update
        self.gui.main_layout.addWidget(self.label)

        self.label.connect_symbol(self.model.MySineWave.Amplitude)

        self.assertIsNotNone(self.label._timer)
        self.assertEqual(self.label._timer.interval(), 6)  # In ms


class TcLineEditTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        self.box = TcLineEdit(greyed=False)  # Do not disabled when disconnected
        self.gui.main_layout.addWidget(self.box)

    def test_write(self):
        self.box.connect_symbol(self.model.MyConstant.Value)

        self.assertIn("#FFFFFF", self.box.styleSheet())  # Assert default color

        # Type inside box and verify remote change
        QTest.mouseClick(self.box, Qt.LeftButton)  # Select it

        self.box.clear()
        QTest.keyClicks(self.box, "123.45")  # Enter text

        self.assertIn("#FFFFAA", self.box.styleSheet())  # Assert editing color

        QTest.keyPress(self.box, Qt.Key_Enter)  # Submit

        self.assertIn("#FFFFFF", self.box.styleSheet())

        new_val = self.model.MyConstant.Value.read()
        self.assertEqual(new_val, 123.45)

    def test_read(self):
        self.box.connect_symbol(self.model.MyConstant.Value)

        # Make remote change and verify box update
        self.model.MyConstant.Value.write(1645.16444)

        self.assertEqual(self.box.text(), "1645.164")

    def test_write_unconnected(self):
        # Type inside box and verify remote change
        QTest.mouseClick(self.box, Qt.LeftButton)  # Select it

        self.box.clear()
        QTest.keyClicks(self.box, "123.45")  # Enter text
        QTest.keyPress(self.box, Qt.Key_Enter)  # Submit

        self.assertEqual("123.45", self.box.text())

    def test_event_loop(self):
        """Verify the update-write-update loop is avoided."""

        self.box.twincat_send = mock.MagicMock(wraps=self.box.twincat_send)
        self.box.twincat_receive = mock.MagicMock(wraps=self.box.twincat_receive)

        self.box.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(420.0)  # Remote change

        # Once received, no send!
        self.box.twincat_send.assert_not_called()
        self.box.twincat_receive.assert_called_once()

        self.box.twincat_send.reset_mock()
        self.box.twincat_receive.reset_mock()

        self.box.clear()
        QTest.keyClicks(self.box, "9494.5")  # Enter text
        QTest.keyPress(self.box, Qt.Key_Enter)  # Local change

        # Not received, sent once!
        self.box.twincat_send.assert_called_once()
        self.box.twincat_receive.assert_not_called()


class TcPushButtonTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        self.button = TcPushButton()
        self.gui.main_layout.addWidget(self.button)

    def test_push(self):

        self.button.connect_symbol(self.model.MyConstant.Value)

        QTest.mousePress(self.button, Qt.LeftButton)  # Hold it
        self.assertEqual(1.0, self.model.MyConstant.Value.read())

        QTest.mouseRelease(self.button, Qt.LeftButton)  # Release it
        self.assertEqual(0.0, self.model.MyConstant.Value.read())

    def test_push_unconnected(self):

        QTest.mousePress(self.button, Qt.LeftButton)  # Hold it
        QTest.mouseRelease(self.button, Qt.LeftButton)  # Release it


class TcRadioButtonTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        self.button = TcRadioButton("x")
        self.gui.main_layout.addWidget(self.button)

    def test_select(self):

        self.button.connect_symbol(self.model.MyConstant.Value)

        # Click the radio
        # QTest.mouseClick(self.button, Qt.LeftButton)  # A bug in PyQt prevents a radio
        # from being clicked, call event directly instead
        self.button.click()

        self.assertEqual(1.0, self.model.MyConstant.Value.read())

    def test_select_unconnected(self):

        self.button.click()

    def test_read(self):

        self.button.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(1.0)

        self.assertTrue(self.button.isChecked())

    def test_read_unknown_value(self):
        """Test what happens when the remote value changes to something unknown."""

        self.button.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(3.14)

        self.assertFalse(self.button.isChecked())


class TcRadioButtonGroupBoxTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        self.box = TcRadioButtonGroupBox(
            options=[
                ("Off", 0.0),
                ("Low", 0.5),
                ("High", 2.0),
            ]
        )
        self.gui.main_layout.addWidget(self.box)

    def test_select(self):

        self.box.connect_symbol(self.model.MyConstant.Value)

        # Click all radios and check output
        # QTest.mouseClick(self.box.radio_buttons[1], Qt.LeftButton)  # A bug in PyQt
        # prevents a radio from being clicked, call event directly instead
        self.box.radio_buttons[1].click()
        self.assertEqual(0.5, self.model.MyConstant.Value.read())

        self.box.radio_buttons[2].click()
        self.assertEqual(2.0, self.model.MyConstant.Value.read())

        self.box.radio_buttons[0].click()
        self.assertEqual(0.0, self.model.MyConstant.Value.read())

    def test_select_unconnected(self):
        self.box.radio_buttons[1].click()
        self.box.radio_buttons[2].click()
        self.box.radio_buttons[0].click()

    def test_read(self):

        self.model.MyConstant.Value.write(0.1)  # Write something else so we can
        # trigger a notification later

        self.assertFalse(self.box.radio_buttons[0].isChecked())
        self.assertFalse(self.box.radio_buttons[1].isChecked())
        self.assertFalse(self.box.radio_buttons[2].isChecked())

        self.box.connect_symbol(self.model.MyConstant.Value)
        self.model.MyConstant.Value.write(0.0)  # In real TwinCAT the current value
        # is fired on creating a callback

        self.assertTrue(self.box.radio_buttons[0].isChecked())
        self.assertFalse(self.box.radio_buttons[1].isChecked())
        self.assertFalse(self.box.radio_buttons[2].isChecked())

        self.model.MyConstant.Value.write(0.5)
        self.assertFalse(self.box.radio_buttons[0].isChecked())
        self.assertTrue(self.box.radio_buttons[1].isChecked())
        self.assertFalse(self.box.radio_buttons[2].isChecked())

        self.model.MyConstant.Value.write(2.0)
        self.assertFalse(self.box.radio_buttons[0].isChecked())
        self.assertFalse(self.box.radio_buttons[1].isChecked())
        self.assertTrue(self.box.radio_buttons[2].isChecked())

    def test_read_unknown_value(self):
        """Test what happens when the remote value changes to something unknown."""

        self.box.connect_symbol(self.model.MyConstant.Value)
        self.model.MyConstant.Value.write(3.14)

        # None should be selected
        self.assertFalse(self.box.radio_buttons[0].isChecked())
        self.assertFalse(self.box.radio_buttons[1].isChecked())
        self.assertFalse(self.box.radio_buttons[2].isChecked())

    def test_event_loop(self):
        """Verify the update-write-update loop is avoided."""

        self.box.twincat_send = mock.MagicMock(wraps=self.box.twincat_send)
        self.box.twincat_receive = mock.MagicMock(wraps=self.box.twincat_receive)

        self.box.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(2.0)  # Remote change

        # Once received, no send!
        self.box.twincat_send.assert_not_called()
        self.box.twincat_receive.assert_called_once()

        self.box.twincat_send.reset_mock()
        self.box.twincat_receive.reset_mock()

        self.box.radio_buttons[1].click()  # QTest doesn't work here

        # Not received, sent once!
        self.box.twincat_send.assert_called_once()
        self.box.twincat_receive.assert_not_called()


class TcCheckBoxTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        self.box = TcCheckBox("Enable motor")
        self.gui.main_layout.addWidget(self.box)

    def test_select(self):

        self.box.connect_symbol(self.model.MyConstant.Value)

        # Click the radio
        # QTest.mouseClick(self.box, Qt.LeftButton)  # A bug in PyQt prevents a radio
        # from being clicked, call event directly instead
        self.box.click()

        self.assertEqual(1.0, self.model.MyConstant.Value.read())

    def test_select_unconnected(self):

        self.box.click()

    def test_read(self):

        self.box.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(1.0)

        self.assertTrue(self.box.isChecked())

    def test_read_unknown_value(self):
        """Test what happens when the remote value is not known by the box."""

        self.box.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(3.14)
        self.assertFalse(self.box.isChecked())


class TcSliderTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        self.slider = TcSlider()
        self.gui.main_layout.addWidget(self.slider)

    def test_select(self):

        self.slider.connect_symbol(self.model.MyConstant.Value)
        self.model.MyConstant.Value.write(0.0)  # In real TwinCAT the current value
        # is fired on creating a callback

        # Click on slider (moves it to the right by `pageSize`)
        QTest.mouseClick(self.slider.slider, Qt.LeftButton)

        self.assertEqual(5.0, self.model.MyConstant.Value.read())

    def test_select_unconnected(self):

        QTest.mouseClick(self.slider.slider, Qt.LeftButton)

    def test_read(self):

        self.slider.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(33.0)

        self.assertEqual(33, self.slider.slider.value())

    def test_select_read_float(self):
        """Test scaled slider, with float output."""

        tc_slider = TcSlider(
            symbol=self.model.MyConstant.Value,
            min=1.0,
            max=4.0,
            interval=0.1,
        )
        self.gui.main_layout.addWidget(tc_slider)

        self.model.MyConstant.Value.write(1.9)
        self.assertEqual(19, tc_slider.slider.value())

        QTest.mouseClick(tc_slider.slider, Qt.LeftButton)
        self.assertEqual(2.4, self.model.MyConstant.Value.read())

    def test_event_loop(self):
        """Verify the update-write-update loop is avoided."""

        self.slider.twincat_send = mock.MagicMock(wraps=self.slider.twincat_send)
        self.slider.twincat_receive = mock.MagicMock(wraps=self.slider.twincat_receive)

        self.slider.connect_symbol(self.model.MyConstant.Value)

        self.model.MyConstant.Value.write(5.0)  # Remote change

        # Once received, no send!
        self.slider.twincat_send.assert_not_called()
        self.slider.twincat_receive.assert_called_once()

        self.slider.twincat_send.reset_mock()
        self.slider.twincat_receive.reset_mock()

        QTest.mouseClick(self.slider.slider, Qt.LeftButton)  # Local change

        # Not received, sent once!
        self.slider.twincat_send.assert_called_once()
        self.slider.twincat_receive.assert_not_called()


class TcGraphTestCase(GuiTestCase):
    def setUp(self):
        super().setUp()

        TcGraph.FPS = 10000.0  # Force fast update

        self.graph = TcGraph(
            buffer_size=10,  # Force a buffer wrap
            labels=["Sum Output"],
            units=["m"]
        )
        self.gui.main_layout.addWidget(self.graph)

    def test_graph(self):

        self.graph.connect_symbol(
            [self.model.MyConstant.Value],
            update_freq=500.0,
        )

        self.sleep_qt(100)  # Should have a couple of samples now

        self.assertEqual((10, 2), self.graph.data.shape)
        # Value column should be all zero:
        self.assertEqual(0.0, np.sum(self.graph.data[:, 1]))
        # Time column should at least not be nan:
        self.assertFalse(np.isnan(
            np.sum(self.graph.data[:, 0])
        ))


if __name__ == "__main__":
    unittest.main()
