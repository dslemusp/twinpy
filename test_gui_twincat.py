import sys
from twinpy.ui.tc_widgets import TcGraph, TcPushButton
from PyQt5.QtWidgets import QApplication, QFormLayout, QLabel, QWidget, QGridLayout
from PyQt5.QtGui import QDoubleValidator

from twinpy.ui import CMGGUI
from twinpy.ui import TcLabel, TcLineEdit
from twinpy.twincat import SimulinkModel
from twinpy.twincat import TwincatConnection



class TestGUI(CMGGUI):
    def __init__(self, controller: SimulinkModel, **kwargs):

        super().__init__(model=controller, **kwargs)

        layout_LC   = QGridLayout()
        Force_col   = QFormLayout()
        Moment_col  = QFormLayout()

        self.zeroLC = TcPushButton(
            symbol=controller.ZeroLC.Value
        )
        self.zeroLC.setText('Test')

        # self.input_box = TcLineEdit(
        #     # symbol=controller.MySineWave.Bias,
        # )
        # self.input_box.setValidator(QDoubleValidator())

        self.label_Fx = TcLabel(
            symbol=controller.ATI_LC1.Fx.so1
        )
        self.label_Fy = TcLabel(
            symbol=controller.ATI_LC1.Fy.so1
        )
        self.label_Fz = TcLabel(
            symbol=controller.ATI_LC1.Fz.so1
        )
        self.label_Mx = TcLabel(
            symbol=controller.ATI_LC1.Tx.so1
        )
        self.label_My = TcLabel(
            symbol=controller.ATI_LC1.Ty.so1
        )
        self.label_Mz = TcLabel(
            symbol=controller.ATI_LC1.Tz.so1
        )

        self.graph_Fx = TcGraph(
            symbol=controller.ATI_LC1.Fx.so1
            # update_freq=1.0
            # buffer_size=1000
        )
        
        Force_col.addRow(QLabel("Fx (N):"), self.label_Fx)
        # Force_col.addRow(QLabel("Fy (N):"), self.label_Fy)
        # Force_col.addRow(QLabel("Fz (N):"), self.label_Fz)
        Moment_col.addRow(QLabel("Mx (Nm):"), self.label_Mx)
        # Moment_col.addRow(QLabel("My (Nm):"), self.label_My)
        # Moment_col.addRow(QLabel("Mz (Nm):"), self.label_Mz)

        layout_LC.addWidget(self.zeroLC,0,0,1,2)
        layout_LC.addLayout(Force_col,1,0)
        layout_LC.addLayout(Moment_col,1,1)
        layout_LC.addWidget(self.graph_Fx,2,0,1,2)

        widget_label = QWidget()
        widget_label.setLayout(layout_LC)
        self.layout_main.addWidget(widget_label)


if __name__ == "__main__":

    # tc = TwincatConnection(TARGET_IP = '192.168.0.100',TARGET_USERNAME = 'localadmin', TARGET_PASSWORD = 'Tjongejonge!')
    tc = TwincatConnection() 

    app = QApplication(sys.argv)

    model = SimulinkModel(0x01010020,object_name = "TestCMG",type_name = "TestCMG", TCconn = tc)

    model.connect_to_twincat(tc)

    gui = TestGUI(model)

    app.exec()
